//
//  ViewController.swift
//  Plano
//
//  Created by Nil Puig on 17/04/2018.
//  Copyright © 2018 Nil Puig. All rights reserved.
//

import UIKit

class TimerViewController: UIViewController {

    @IBOutlet weak var timerLabel: UILabel!
    @IBOutlet weak var startStopBtn: UIButton!
    
    let defaults = UserDefaults.standard
    weak var timer: Timer?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initialSetupTimer1()
        initialSetupTimer2()
    }
    
    // MARK:- TIMER 1

    
    func initialSetupTimer1() {
        let timeIsRunning = defaults.bool(forKey: "timeIsRunning")
        if timeIsRunning {
            self.timerLabel.text = "0"
            startTimer()
            self.startStopBtn.setTitle("STOP", for: .normal)
        } else{
            self.startStopBtn.setTitle("START", for: .normal)
            self.timerLabel.text = "0"
        }
    }
    
    
    @IBAction func startStopTimer(_ sender: Any) {
        let initialTime = getCurrentTime()
        defaults.set(initialTime, forKey: "initialTime")
        
        let timeIsRunning = defaults.bool(forKey: "timeIsRunning")
        if timeIsRunning {
            print("stop")
            self.startStopBtn.setTitle("START", for: .normal)
            defaults.set(false, forKey: "timeIsRunning")
            stopTimer()
            self.timerLabel.text = "0"
        } else{
            print("start")
            self.startStopBtn.setTitle("STOP", for: .normal)
            defaults.set(true, forKey: "timeIsRunning")
            startTimer()
        }
    }


    func showTimer() {
        let initialTime = defaults.integer(forKey: "initialTime")
        let currentTime = getCurrentTime()
        let timer = currentTime - initialTime
        self.timerLabel.text = "\(timer)"
    }
    
    func startTimer() {
        timer?.invalidate()   // just in case you had existing `Timer`, `invalidate` it before we lose our reference to it
        let initialTime = defaults.integer(forKey: "initialTime")
        timer = Timer.scheduledTimer(withTimeInterval: 1.0, repeats: true) { [weak self] _ in
            if let strongSelf = self {
                let currentTime = strongSelf.getCurrentTime()
                let timer = currentTime - initialTime
                strongSelf.timerLabel.text = "\(timer)"
                
            }
        }
    }
    
    func getCurrentTime() -> Int {
        let timeInterval = NSDate().timeIntervalSince1970
        return Int(timeInterval)
    }
    
    func stopTimer() {
        timer?.invalidate()
    }
    
    
    // MARK:- TIMER 2
    
    @IBOutlet weak var timerLabel2: UILabel!
    @IBOutlet weak var startStopBtn2: UIButton!
    var time: UInt = 1
    var isSelected = false
    var updateTimer: Timer?
    var backgroundTask: UIBackgroundTaskIdentifier = UIBackgroundTaskInvalid
    
    func initialSetupTimer2() {
        self.timerLabel2.text = "0"
        NotificationCenter.default.addObserver(self, selector: #selector(reinstateBackgroundTask), name: NSNotification.Name.UIApplicationDidBecomeActive, object: nil)
    }

    @objc func reinstateBackgroundTask() {
        if updateTimer != nil && (backgroundTask == UIBackgroundTaskInvalid) {
            registerBackgroundTask()
        }
    }
    
    @IBAction func startStopTimer2(_ sender: UIButton) {
        isSelected = !isSelected
        if isSelected {
            self.startStopBtn2.setTitle("STOP", for: .normal)
            time = 1
            updateTimer = Timer.scheduledTimer(timeInterval: 1.0, target: self,
                                               selector: #selector(calculateTime), userInfo: nil, repeats: true)
            registerBackgroundTask()
        } else {
            self.timerLabel2.text = "0"
            self.startStopBtn2.setTitle("START", for: .normal)
            updateTimer?.invalidate()
            updateTimer = nil
            if backgroundTask != UIBackgroundTaskInvalid {
                endBackgroundTask()
            }
        }
    }
    
    func registerBackgroundTask() {
        print("registerBackgroundTask")
        backgroundTask = UIApplication.shared.beginBackgroundTask { [weak self] in
            self?.endBackgroundTask()
        }
        assert(backgroundTask != UIBackgroundTaskInvalid)
    }
    
    func endBackgroundTask() {
        print("Background task ended.")
        UIApplication.shared.endBackgroundTask(backgroundTask)
        backgroundTask = UIBackgroundTaskInvalid
    }
    
    @objc func calculateTime() {
        print("calculateTime")
        
        switch UIApplication.shared.applicationState {
        case .active:
            timerLabel2.text = "\(time)"
        case .background:
            print("App is backgrounded. Time = \(time)")
            print("Background time remaining = \(UIApplication.shared.backgroundTimeRemaining) seconds")
        case .inactive:
            break
        }
        
        time += 1
    }
    
    
    deinit {
        stopTimer()
        NotificationCenter.default.removeObserver(self)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    

}

