//
//  ViewController.swift
//  Plano
//
//  Created by Nil Puig on 17/04/2018.
//  Copyright © 2018 Nil Puig. All rights reserved.
//

import UIKit

class MemoryViewController: UIViewController {

    weak var timer: Timer?
    @IBOutlet weak var startStopButton: UIButton!
    @IBOutlet weak var puppiesLabel: UILabel!
    var isRunning = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        startTimer()
        
        startStopButton.alpha = 0.0
        UIView.animate(withDuration: 3.5, animations: {
            self.startStopButton.alpha = 1.0
        })
    }
    
    // Screen width.
    public var screenWidth: CGFloat {
        return UIScreen.main.bounds.width
    }
    
    // Screen height.
    public var screenHeight: CGFloat {
        return UIScreen.main.bounds.height
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
        self.presentAlert(title:"High memory usage!")
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0, execute: {
            self.dismissCustomAlert()
        })
        
        // Dispose of any resources that can be recreated.
    }
    
    func startTimer() {
        print("startTimer")
        
        UIView.animate(withDuration: 3.5, animations: {
            self.puppiesLabel.alpha = 0.0
        })
        
        timer?.invalidate()   // just in case you had existing `Timer`, `invalidate` it before we lose our reference to it
        timer = Timer.scheduledTimer(withTimeInterval: 0.5, repeats: true) { [weak self] _ in
            if let strongSelf = self {
                var i = 0
                while (i<3) {
                    let imageName = "dog.png"
                    let image = UIImage(named: imageName)
                    let imageView = UIImageView(image: image!)

                    let url = URL(string: "https://images.pexels.com/photos/39317/chihuahua-dog-puppy-cute-39317.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260")
                    let data = try? Data(contentsOf: url!) //make sure your image in this url does exist, otherwise unwrap in a if let check / try-catch
                    imageView.image = UIImage(data: data!)
                    
                    let xPos = Int(arc4random_uniform(UInt32(strongSelf.screenWidth)-200))
                    var yPos = Int(arc4random_uniform(UInt32(strongSelf.screenHeight-200)))
                    if (yPos < 100) {
                        yPos += 150
                    }
                    imageView.frame = CGRect(x: xPos, y: yPos, width: 200, height: 200)
                    strongSelf.view.addSubview(imageView)


                    UIView.animate(withDuration: 3.5, animations: {
                        imageView.alpha = 0.0
                    })
                    i += 1
                }
                
            }
        }
    }
    
    func getCurrentTime() -> Int {
        let timeInterval = NSDate().timeIntervalSince1970
        return Int(timeInterval)
    }
    
    func stopTimer() {
        print("stopTimer")
        
        timer?.invalidate()
    }
    
    // if appropriate, make sure to stop your timer in `deinit`
    
    deinit {
        stopTimer()
    }
    
    @IBAction func releaseMemory(_ sender: Any) {
        if (!isRunning) {
            startStopButton.setTitle( "START AGAIN" , for: .normal )
            stopTimer()
            self.allSubViews(views: self.view.subviews)
            isRunning = true
            self.puppiesLabel.alpha = 1.0
        } else {
            startTimer()
            startStopButton.setTitle( "STOP" , for: .normal )
            isRunning = false
        }
        
    }
    
    func allSubViews(views: [UIView]) {
        for view in views {
            if let tf = view as? UIImageView {
                tf.image = nil
            }
            self.allSubViews(views: view.subviews)
        }
    }


}

