//
//  File.swift
//  Plano
//
//  Created by Nil Puig on 17/04/2018.
//  Copyright © 2018 Nil Puig. All rights reserved.
//

import UIKit

extension UITabBarItem {
    override open func awakeFromNib() {
        super.awakeFromNib()
        titlePositionAdjustment = UIOffset(horizontal: 0, vertical: -2)
    }
}

