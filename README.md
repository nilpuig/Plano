Plano Test
======



## Questions

## **1) How long do you work in swift?**

For about 3 years. I started after a couple of months  swift 2.0 was  released.
    
## **2) Can you share your experience in dealing with core native functions?**

I think the best way to answer this is by giving you examples of some previous work. This is my github profile: https://github.com/NilPuig and you can check a previous test I did here:  https://github.com/NilPuig/Skyscanner-Test

## **3) How do you handle runtime memory? How do you do memory management?**

First, I need to take into account the five stages of an object lifetime for swift / objective c :

1.  Allocation (memory taken from stack or heap)
    
2.  Initialization (init code runs)
    
3.  Usage (the object is used)
    
4.  Deinitialization (deinit code runs)
    
5.  Deallocation (memory returned to stack or heap)

**Swift**
In the case of Swift,  much of the memory management is done by ARC, so it's more simple than Objective C, but I still need to consider relationships between objects to avoid memory leaks.

**Memory leaks**
Strong reference cycle happen when two objects are no longer required, but each reference one another. Since each has a non-zero reference count, deallocation, of both objects, can never occur. It fools ARC and prevents it from cleaning up.
    

-   To break strong reference cycles, I specify the relationship between reference counted objects as `weak`. This way I don't increase the strong reference count of the object. Another way to do it is by using `unowned`. 

A useful tool for memory management is the Debug Memory Graph on Xcode. Once I open it, I look for the purple squares with a exclamation marks. Finally I found the relationship between the 2 objects and make it weak.
    
## 4) Can you explain the IOS life cycle?
    
Yes, I made the following summary for the iOS life cycle a couple of months ago, so I'm going to use it to answer the question:

There are 2 main lifecycles:

-   The App life cycle
    
-   The View Controller life cycle
    

### App life cycle

####  The main() function

When the user taps on the app icon, the iOS system creates a process and a main thread for the app and runs the app’s main() function on that main thread.

That’s because the execution of any C program starts with a function called main(), and since Objective-C is a strict superset of C, the same must be true for an Objective-C program. This is the entire code of main:

  

    int main(int argc, char * argv[]) {  
	    @autoreleasepool {  
	    return  UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));  
	    }  
    }

 

 The program ends when it reaches the end of main(). However, UIApplicationMain()function never returns, therefore the program never ends.

  

#### The UIApplicationMain() funtion

Now let’s explain in detail what is the function of UIApplicationMain(). Once it’s called, it creates the app’s main object UIApplication, sets the UIApplication for the application, loads the app’s user interface from the available storyboard files, and puts the app’s run loop in motion that is used by the UIApplication instance to process events such as touches or network events. The run loop is basically an infinite loop that causes UIApplicationMain()to never return. Before UIApplication processes the first event, it finally sends the application:didFinishLaunchingWithOptions:  message to its delegate, giving us the chance to do our own setup.

Here is a visual represention of these events:

![](https://lh4.googleusercontent.com/AJ-j_97h5U7FkxHgTwVDLmosqDwGqvlV3JmqwXWeh49WVJypO5zDe-szbJEy12aHhwDcgC5BfD3vgr72YO2TPBf7PY0R1iwGwQtiY5ewQ1KGgzppV86Rm8GBN5mqa__1mJt2D-TB)

  

#### The main run loop

The UIApplication object sets up the main run loop at launch time and uses it to process all user-related events and handle updates to view-based interfaces. As the name suggests, the main run loop executes on the app’s main thread. This behavior ensures that user-related events are processed serially in the order in which they were received.

The most common types of events are: touch, location, remote control, accelerometer, gyroscope, magnetometer and redraw.

  

#### The app states

After application:didFinishLaunchingWithOptions:  is called, the app will call a other methods in the delegate every time there’s a transition to another state.

Here is a visual represention of the iOS app states:

![](https://lh6.googleusercontent.com/Ykw_e5as7coCVQU2CIXLK8DOaZwba-kyaOrLyLFIbUm_zxmFHLWg7cbb3BaUKorqJ6iyAfU8yUDepoIrDgjtzUC2lNvgItW_vGB4kxdkgGLnm489im1aDXQ4raTWrmlnytL8rjRm)

  

And these are the methods that respond to state changes:

-   application:willFinishLaunchingWithOptions:—This method is your app’s first chance to execute code at launch time.
    
-   application:didFinishLaunchingWithOptions:—This method allows you to perform any final initialization before your app is displayed to the user.
    
-   applicationDidBecomeActive:—Lets your app know that it is about to become the foreground app. Use this method for any last minute preparation.
    
-   applicationWillResignActive:—Lets you know that your app is transitioning away from being the foreground app. Use this method to put your app into a quiescent state.
    
-   applicationDidEnterBackground:—Lets you know that your app is now running in the background and may be suspended at any time.
    
-   applicationWillEnterForeground:—Lets you know that your app is moving out of the background and back into the foreground, but that it is not yet active.
    
-   applicationWillTerminate:—Lets you know that your app is being terminated. This method is not called if your app is suspended.
    

  

#### View Controller life cycle

If you are creating your UI or View programmatically then the first method which gets called is loadView, followed by viewDidLoad -> viewWillAppear -> viewDidAppear. When moving to another next screen, the following methods are called: viewWillDisappear -> viewDidDisappear -> viewDidUnload.

![](https://lh5.googleusercontent.com/TawHO8dZjWaLmTcCwhbFii5R3w5OJpAIpwZLCRVsCXIpsgGKlumbx9bAkxxNk8xB_zEqJ3e_L7SpM6gLNMyhDF3k7WJagkg_f1H3HGAAo_j0FjQunuWyniea-tMgxiyFELdraTGG)

Lets describe each method:

-   viewDidLoad()—Called when the view controller’s content view (the top of its view hierarchy) is created and loaded from a storyboard. The view controller’s outlets are guaranteed to have valid values by the time this method is called. Use this method to perform any additional setup required by your view controller.
    
-   viewWillAppear()—Called just before the view controller’s content view is added to the app’s view hierarchy. Use this method to trigger any operations that need to occur before the content view is presented onscreen.
    
-   viewDidAppear()—Called just after the view controller’s content view has been added to the app’s view hierarchy. Use this method to trigger any operations that need to occur as soon as the view is presented onscreen, such as fetching data or showing an animation.
    

- viewWillDisappear()—Called just before the view controller’s content view is removed from the app’s view hierarchy. Use this method to perform cleanup tasks like committing changes or resigning the first responder status. viewDidDisappear()—Called just after the view controller’s content view has been removed from the app’s view hierarchy. Use this method to perform additional teardown activities.

## 5) How do you use auto layout for doing iPhone and iPad apps? How do you do view controllers and corresponding storyboard elements? 

As this has to do with practice more than theory I would recommend you check my previous work. I always use auto layout and constraints in my projects since its introduction a few years ago. The most controllers I used are the view controllers, tableviewcontrollers, collection view controller, navigation controller and tab view controller .
    
## 6.   Can IOS app work in background? If so how is it done?

Yes, by using background modes. To get to the background modes capability list you (1) select the project from the Project Navigator, (2) select the app target, (3) select the Capabilities tab, and (4) turn the Background Modes switch on.
    
## 7.  Explain how will you do image optimization?

For uploading the image to the server I would compress the image before and convert it to the right format.

For downloading images I usually use this framework: https://github.com/pinterest/PINRemoteImage 


## Code Test

1) In the second tab of my app I download images until the user presses STOP or the memory is so high that the app crashes
2) There are 2 timers, one works using userdefaults, it will always show the time and doesn't require to work on the backgroud. The second timer (on the bottom), works on the foreground and background. I use what is called "Executing a Finite-Length Task in the Background". Technically, is not a background mode, as I don't need to select any background mode in capabilities to make it work. Instead, I call some methods that let me run a code for a finite amount of time.
